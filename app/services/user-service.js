import Ember from 'ember';

export default Ember.Service.extend({
  session: Ember.inject.service(),
  store: Ember.inject.service(),
  init(){
    this.getUser();
  },

  getUser(){
    let uid = this.get('session.uid');

    // return this.get('store').findAll('user', {
    //
    // })
    // return this.get('store').query('user', {
    //   orderBy: 'uid',
    //   equalTo: uid
    // });
    //
    // return new Promise((resolve)=>{
      this.get('store').query('user', {
        orderBy: 'uid',
        equalTo: uid
      }).then(res=>{
        console.log(res)
        this.set('user', res.get('firstObject'));
        // resolve(res.get('firstObject'));
      })
    // });

  },
  // getFavorites(){
  //   this.getUser().then(res=>{
  //     this.set('favorites', res.get('favorites'))
  //   })
  // },

  addToFavorites(item){
    let genres;

    let data = {
      id: item.id,
      title: item.title,
      poster_path: item.poster_path,
      release_date: item.release_date,
      overview: item.overview
    }
    if(item.genres){
      genres = item.genres.map(i=>{
        return i.id;
      })
      data.genre_ids = genres;
    }

    console.log(data)
    let favorites = this.get('user.favorites') || this.set('user.favorites', [])
    favorites.pushObject(data);
    console.log(this.get('user'), favorites)
    this.get('user').save();
    // this.getUser().then(res=>{
    //
    //   res.get('favorites').pushObject(item);
    //   res.save();
    // })

    // let uid = this.get('session.uid');
    //
    // let user = this.get('store').createRecord('user', {
    //   uid: uid,
    //   favorites: [item]
    // })
    // user.save();


    // let favorite = this.get('store').createRecord('favorite', {
    //   uid: uid,
    //   tmdb_id: item.id,
    //   title: item.title,
    //   poster_path: item.poster_path,
    //   overview: item.overview,
    //   release_date: item.release_date
    //   // genres: item.genres
    // });
    //
    // favorite.save();
    //
  }
});
