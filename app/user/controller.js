import Ember from 'ember';
import DS from 'ember-data';
import pagedArray from 'ember-cli-pagination/computed/paged-array';

export default Ember.Controller.extend({
  userService: Ember.inject.service(),
  favorites: Ember.computed('userService.user', function(){
    return this.get('userService.user.favorites');
  })
});
